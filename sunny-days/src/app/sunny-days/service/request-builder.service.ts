import { Injectable } from '@angular/core';
import { Params } from '../query/request/params';

@Injectable({
  providedIn: 'root'
})
export class RequestBuilderService {
  URL: string = 'http://api.openweathermap.org/data/2.5/';
  key: string = '8a6bae93d9b395d0026a6aa66f05aba3';
  request: string = 'http://api.openweathermap.org/data/2.5/';
  params: Params = new Params();
  constructor() { }
  init() {
    this.request = this.URL;
  }
  oneCallForecast(lat: number, lon: number) {
    this.request += 'onecall?lat=' + lat + '&lon=' + lon;
  }
  oneCallHistorical(lat: number, lon: number, dt: number) {
    this.request += 'onecall/timemachine?lat=' + lat + '&lon=' + lon + '&dt=' + dt;
  }
  group(cities: number[]) {
    this.request += 'group?id='
    cities.forEach(city => {
      this.request += city + ',';
    });
    this.request.slice(0, -1);
  }
  findByLatLon(lat: number, lon: number) {
    this.request += 'find?lat=' + lat + '&lon=' + lon + '&cnt=1';
  }
  find(city: string) {
    this.request += 'find?q=' + city;
  }
  weather(city: string) {
    this.request += 'weather?q=' + city;
  }
  forecast(city: number) {
    this.request += 'forecast/daily?id=' + city;
  }
  lang() {
    this.request += '&lang=' + this.params.lang;
  }
  units() {
    this.request += '&units=' + this.params.units;
  }
  appendKey() {
    this.request += '&appid=' + this.key;
  }
  fullURL(): string {
    this.lang();
    this.units();
    this.appendKey();
    return this.request;
  }
  noKeyURL(): string {
    this.lang();
    this.units();
    return this.request;
  }
}
