import { Injectable } from '@angular/core';
import { CurrentWeather } from '../query/response/current-weather';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HourlyForecast } from '../query/response/hourly-forecast';
import { CitySearchResponse } from '../query/response/city-search-response';
import { OneCallForecast } from '../query/response/one-call-forecast';
import { OneCallHistorical } from '../query/response/one-call-historical';

@Injectable({
  providedIn: 'root'
})
export class OwmApiService {

constructor(private httpClient: HttpClient) { }

currentWeather(url: string): Observable<CurrentWeather> {
  return this.httpClient.get<CurrentWeather>(url);
}

hourlyForecast(url: string): Observable<HourlyForecast> {
  return this.httpClient.get<HourlyForecast>(url);
}

oneCallForecast(url: string): Observable<OneCallForecast> {
  return this.httpClient.get<OneCallForecast>(url);
}

oneCallHistorical(url: string): Observable<OneCallHistorical> {
  return this.httpClient.get<OneCallHistorical>(url);
}

cities(url: string): Observable<CitySearchResponse> {
  return this.httpClient.get<CitySearchResponse>(url);
}

}
