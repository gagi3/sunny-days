import { Injectable } from '@angular/core';
import { Cities } from '../query/response/cities';

const CITY = 'City';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private pinned: Cities[] = [];

constructor() { }

pin(city: Cities) {
  if (!this.pinned.includes(city)) {
    console.log(this.pinned);
    this.pinned.push(city);
    console.log(this.pinned);
    this.addPinned(this.pinned);
  }
}

unpin(city: Cities) {
  let index = this.pinned.indexOf(city, 0);
  if (index > -1) {
    this.pinned.splice(index, 1);
  }
}

addPinned(cities: Cities[]) {
  console.log(this.pinned);
  this.pinned = [...cities];
  console.log(this.pinned);
  localStorage.setItem(CITY, JSON.stringify(this.pinned));
}

getPinned() {
  this.pinned = [];
  if (localStorage.getItem(CITY)) {
    JSON.parse(localStorage.getItem(CITY)).forEach(city => this.pinned.push(city));
  }
  return this.pinned;
}

removePinned(city: Cities) {
  this.removeAll();
  this.pinned = this.pinned.filter(
    ct => ct.name !== city.name
  );
  this.addPinned(this.pinned);
}
  removeAll() {
    localStorage.removeItem(CITY);
  }

}
