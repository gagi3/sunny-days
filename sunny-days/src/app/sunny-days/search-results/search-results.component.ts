import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Cities } from '../query/response/cities';
import { OneCallForecast } from '../query/response/one-call-forecast';
import { OwmApiService } from '../service/owm-api.service';
import { RequestBuilderService } from '../service/request-builder.service';
import { StorageService } from '../service/storage.service';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {
  value = '';
  @Input() cities: Cities[];
  // @Output() pinned = new EventEmitter<Cities[]>();
  @Output() forecast = new EventEmitter<OneCallForecast>();
  @Output() city = new EventEmitter<Cities>();
  pinned: Cities[] = [];

  ngOnInit() {
    if (this.pinned == undefined || this.pinned.length == 0) {
      this.pinned = this.storage.getPinned();
      this.cities = this.pinned;
    }
    this.update();
  }
  constructor(private api: OwmApiService, private requestBuilder: RequestBuilderService, private storage: StorageService) { }

  getForecast(city: Cities) {
    this.requestBuilder.init();
    this.requestBuilder.oneCallForecast(city.coord.lat, city.coord.lon);
    let request = this.requestBuilder.fullURL();
    this.api.oneCallForecast(request).subscribe(
      forecast => {
        this.forecast.emit(forecast);
        this.city.emit(city);
      }
    )
  }

  update() {
    this.cities.forEach(element => {
      this.requestBuilder.init();
      this.requestBuilder.findByLatLon(element.coord.lat, element.coord.lon);
      let request = this.requestBuilder.fullURL();
      this.api.cities(request).subscribe(
        city => {
          element.dt = city.list[0].dt * 1000;
          element.main = city.list[0].main;
          element.weather = city.list[0].weather;
        }
      );
    });
  }

  date(dt: number) {
    let dat = new Date(dt);
    dat.setHours(dat.getHours() + 2);
    return dat.toUTCString().substr(17, 5);
  }

  search() {
    if (this.pinned == undefined || this.pinned.length == 0) {
      this.pinned = this.storage.getPinned();
    }
    this.requestBuilder.init();
    this.requestBuilder.find(this.value);
    var request = this.requestBuilder.fullURL();
    this.api.cities(request).subscribe(
      cities => {
        this.cities = cities.list;
        if (this.pinned != undefined || this.pinned.length == 0) {
          this.pinned.forEach(element => {
            this.cities.push(element);
          })
        }
      }
    )
  }

  clear() {
    this.cities = this.pinned;
  }

  isPinned(city: Cities) {
    let flag = false;
    this.storage.getPinned().forEach(ct => {
      if (ct.id == city.id) {
        flag = true;
      }
    });
    return flag;
  }

  pin(city: Cities) {
    console.log(this.isPinned(city));
    if (this.isPinned(city)) {
      console.log("Pinned!");
      return;
    }
    if (this.storage.getPinned().includes(city)) {
      let index = this.pinned.indexOf(city, 0);
      if (index > -1) {
        this.pinned.splice(index, 1);
      }
      this.storage.removePinned(city);
    } else {
      // this.pinned.push(city);
      this.storage.pin(city);
      this.pinned = this.storage.getPinned();
    }
  }

  unpin(city: Cities) {
    let index = this.pinned.indexOf(city, 0);
      if (index > -1) {
        this.pinned.splice(index, 1);
      }
      this.storage.removePinned(city);
  }

}
