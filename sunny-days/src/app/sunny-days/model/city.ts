export class City {
  id: BigInteger;
  name: String;
  coord: Coordinates;
  country: String;
  population: number;
  timezone: number;
  sunrise: BigInteger;
  sunset: BigInteger;
}
