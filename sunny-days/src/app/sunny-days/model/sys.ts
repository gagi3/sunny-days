export class Sys {
  type: number;
  id: BigInteger;
  message: number;
  country: string;
  sunrise: BigInteger;
  sunset: BigInteger;
  pod: string;
}
