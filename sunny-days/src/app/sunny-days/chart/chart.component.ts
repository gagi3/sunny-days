import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, ViewEncapsulation, ViewChildren } from '@angular/core';
import { Cities } from '../query/response/cities';
import * as d3 from 'd3';
import { OneCallForecast } from '../query/response/one-call-forecast';
import { Current } from '../query/response/current';

@Component({
  selector: 'app-chart',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  @Input() chart: OneCallForecast;
  src = 'Daily';
  tp = 'Temperature';
  title = 'Chart';
  type = 'LineChart';
  data = [];
  start = 0;
  end = 0;
  columnNames = ["Day", "Min temperature", "Max temperature"];
  options = {
     hAxis: {
        title: 'Day'
     },
     vAxis:{
        title: 'Temperature'
     },
   pointSize:5
  };
  width = 550;
  height = 400;

  constructor() {}
  ngOnInit(): void {
    this.chart.daily.forEach(element => {
      let line = [];
      line.push(new Date(element.dt).toUTCString().substr(0,11), element.temp.min, element.temp.max);
      this.data.push(line);
    });
    console.log(this.data);
  }
  selectSource(src: string, type: string, start: number, end: number) {
    console.log(this.chart);
    let data = [];
    this.data = [];
    switch(type) {
      case 'Daily': {
        data = this.chart.daily;
        break;
      }
      case 'Hourly': {
        data = this.chart.hourly;
        break;
      }
      default: {
        data = this.chart.daily;
        break;
      }
    }
    switch(src) {
      case 'Temperature': {
        data.forEach(element => {
          let line = [];
          if(type === 'Daily') {
            line.push(new Date(element.dt).toUTCString().substr(0, new Date(element.dt).toUTCString().length - 4), element.temp.max, element.temp.min);
          } else {
            line.push(new Date(element.dt).toUTCString().substr(0, new Date(element.dt).toUTCString().length - 4), element.temp);
          }
          this.data.push(line);
        });
        this.options.vAxis.title = 'Temperature';
        break;
      }
      case 'Humidity/Cloudiness': {
        data.forEach(element => {
          let line = [];
          line.push(new Date(element.dt).toUTCString().substr(0, new Date(element.dt).toUTCString().length - 4), element.humidity, element.clouds);
          this.data.push(line);
        });
        this.options.vAxis.title = 'Humidity/Cloudiness';
        break;
      }
      case 'Pressure': {
        data.forEach(element => {
          let line = [];
          line.push(new Date(element.dt).toUTCString().substr(0, new Date(element.dt).toUTCString().length - 4), element.pressure);
          this.data.push(line);
        });
        this.options.vAxis.title = 'Pressure';
        break;
      }
      case 'UV Index': {
        data.forEach(element => {
          let line = [];
          line.push(new Date(element.dt).toUTCString().substr(0, new Date(element.dt).toUTCString().length - 4), element.uvi);
          this.data.push(line);
        });
        this.options.vAxis.title = 'UV Index';
        break;
      }
      case 'Precipitation': {
        data.forEach(element => {
          let line = [];
          line.push(new Date(element.dt).toUTCString().substr(0, new Date(element.dt).toUTCString().length - 4), element.rain);
          this.data.push(line);
        });
        this.options.vAxis.title = 'Precipitation';
        break;
      }
      default: {
        data.forEach(element => {
          let line = [];
          if(type === 'Daily') {
            line.push(new Date(element.dt).toUTCString().substr(0, new Date(element.dt).toUTCString().length - 4), element.temp.max, element.temp.min);
          } else {
            line.push(new Date(element.dt).toUTCString().substr(0, new Date(element.dt).toUTCString().length - 4), element.temp);
          }
          this.data.push(line);
        });
        this.options.vAxis.title = 'Temperature';
        break;
      }
    }
    data = [];
    this.data = this.data.slice(start, end);
    // this.data.slice(start, end);
    console.log(this.data);
  }

}
