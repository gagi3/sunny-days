import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { CityComponent } from './city/city.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { SettingsComponent } from './settings/settings.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Observable } from 'rxjs';
import { ThemeService } from './service/theme.service';
import { Cities } from './query/response/cities';
import { OneCallForecast } from './query/response/one-call-forecast';
import { OneCallHistorical } from './query/response/one-call-historical';
import { ForecastComponent } from './forecast/forecast.component';
import { ChartComponent } from './chart/chart.component';

@Component({
  selector: 'app-sunny-days',
  templateUrl: './sunny-days.component.html',
  styleUrls: ['./sunny-days.component.scss']
})
export class SunnyDaysComponent implements OnInit {
  isDarkTheme: Observable<boolean>;
  imgsrc = "../../assets/sunny-days.png";
  @Input() clicked = 'search-results';
  @Input() isAlternateMode = false;
  @ViewChild('city', {static: false}) cityComponent: CityComponent;
  @ViewChild('search-bar', {static: false}) searchBarComponent: SearchBarComponent;
  @ViewChild('search-results', {static: false}) searchResultsComponent: SearchResultsComponent;
  @ViewChild('settings', {static: false}) settingsComponent: SettingsComponent;
  @ViewChild('welcome', {static: false}) welcomeComponent: WelcomeComponent;
  @ViewChild('forecast', {static: false}) forecastComponent: ForecastComponent;
  @ViewChild('chart', {static: false}) chartComponent: ChartComponent;
  public cities: Cities[];
  public forecast: OneCallForecast;
  public city: Cities;
  public chart: OneCallForecast;

  constructor(overlayContainer: OverlayContainer, private themeService: ThemeService) {
    // overlayContainer.getContainerElement().classList.add('sunny-days-dark-theme');
  }

  toggleDarkTheme(checked: boolean) {
    if (checked === true) {
      this.imgsrc = "../../assets/sunny-days-light.png";
    } else {
      this.imgsrc = "../../assets/sunny-days.png";
    }
    this.themeService.setDarkTheme(checked);
  }
  citiesClicked(cities: Cities[]) {
    this.cities = cities;
    this.onClick('search-results');
  }
  forecastClicked(forecast: OneCallForecast) {
    this.forecast = forecast;
    this.onClick('forecast');
  }
  cityClicked(city: Cities) {
    this.city = city;
  }
  chartClicked(chart: OneCallForecast) {
    this.chart = chart;
    this.onClick('chart');
  }
  ngOnInit() {
    this.isDarkTheme = this.themeService.isDarkTheme;
  }

  onClick(click: string) {
    this.clicked = click;
  }



}
