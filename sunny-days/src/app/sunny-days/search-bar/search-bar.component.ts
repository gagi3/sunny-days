import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { OwmApiService } from '../service/owm-api.service';
import { RequestBuilderService } from '../service/request-builder.service';
import { Cities } from '../query/response/cities';
import { StorageService } from '../service/storage.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  value = '';
  pinned: Cities[];
  @Output() cities = new EventEmitter<Cities[]>();
  constructor(private api: OwmApiService, private requestBuilder: RequestBuilderService, private storage: StorageService) { }

  ngOnInit() {
  }
  search() {
    this.pinned = this.storage.getPinned();
    this.requestBuilder.init();
    this.requestBuilder.find(this.value);
    var request = this.requestBuilder.fullURL();
    this.api.cities(request).subscribe(
      cities => {
        var list = [];
        cities.list.forEach(element => {
          list.push(element);
        });
        if (this.pinned != undefined) {
          if (this.pinned.length == 1) {
            list.push(this.pinned[0]);
          } else {
            this.pinned.forEach(element => {
              list.push(element)
            });
          }
        }
        this.cities.emit(list);
      }
    )
  }

}
