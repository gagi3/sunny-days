import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SunnyDaysComponent } from './sunny-days.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon'
import { CityComponent } from './city/city.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { SettingsComponent } from './settings/settings.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { ForecastComponent } from './forecast/forecast.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ChartComponent } from './chart/chart.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatToolbarModule,
    MatIconModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatCheckboxModule,
    GoogleChartsModule,
    MatOptionModule,
    MatSelectModule
  ],
  declarations: [SunnyDaysComponent, CityComponent, SearchBarComponent, SearchResultsComponent, SettingsComponent, WelcomeComponent, ForecastComponent, ChartComponent]
})
export class SunnyDaysModule { }
