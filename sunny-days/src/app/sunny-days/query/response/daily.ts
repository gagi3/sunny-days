import { Weather } from '../../model/weather';
import { Temperature } from '../../model/temperature';
import { FeelsLike } from '../../model/feels-like';
import { Rain } from '../../model/rain';

export class Daily {
  dt: number;
  sunrise: number;
  sunset: number;
  temp: Temperature;
  feels_like: FeelsLike;
  pressure: number;
  humidity: number;
  dew_point: number;
  uvi: number;
  clouds: number;
  wind_speed: number;
  wind_deg: number;
  weather: Weather[];
  rain: number;
}
