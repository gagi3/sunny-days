import { Main } from '../../model/main';
import { Wind } from '../../model/wind';
import { Sys } from '../../model/sys';
import { Rain } from '../../model/rain';
import { Snow } from '../../model/snow';
import { Clouds } from '../../model/clouds';
import { Weather } from '../../model/weather';
import { Coordinates } from '../../model/coordinates';

export class Cities {
  id: number;
  name: String;
  coord: Coordinates;
  main: Main;
  dt: number;
  wind: Wind;
  sys: Sys;
  rain: Rain;
  snow: Snow;
  clouds: Clouds;
  weather: Weather[];
}
