import { Coordinates } from '../../model/coordinates';
import { Weather } from '../../model/weather';
import { Main } from '../../model/main';
import { Wind } from '../../model/wind';
import { Clouds } from '../../model/clouds';
import { Sys } from '../../model/sys';

export class CurrentWeather {
  coord: Coordinates;
  weather: Weather[];
  base: string;
  main: Main;
  visibility: number;
  wind: Wind;
  clouds: Clouds;
  dt: BigInteger;
  sys: Sys;
  timezone: number;
  id: BigInteger;
  name: string;
  cod: number;
}
