import { Cities } from './cities';

export class CitySearchResponse {
  message: string;
  cod: string;
  count: number;
  list: Cities[];
}
