import { Current } from './current';

export class OneCallHistorical {
  lat: number;
  lon: number;
  timezone: string;
  current: Current;
  hourly: Current[];
}
