import { HourlyDataList } from './hourly-data-list';
import { City } from '../../model/city';

export class HourlyForecast {
  cod: string;
  message: number;
  cnt: number;
  list: HourlyDataList[];
  city: City;
}
