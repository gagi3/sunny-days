import { Main } from '../../model/main';
import { Weather } from '../../model/weather';
import { Clouds } from '../../model/clouds';
import { Wind } from '../../model/wind';
import { Rain } from '../../model/rain';
import { Snow } from '../../model/snow';
import { Sys } from '../../model/sys';

export class HourlyDataList {
  dt: BigInteger;
  main: Main;
  weather: Weather[];
  clouds: Clouds;
  wind: Wind;
  rain: Rain;
  snow: Snow;
  sys: Sys;
  dt_txt: string;
}
