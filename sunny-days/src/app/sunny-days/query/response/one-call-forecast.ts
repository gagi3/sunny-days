import { Weather } from '../../model/weather';
import { Current } from './current';
import { Daily } from './daily';

export class OneCallForecast {
  lat: number;
  lon: number;
  timezone: string;
  current: Current;
  hourly: Current[];
  daily: Daily[];
}
