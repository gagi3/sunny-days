import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OneCallForecast } from '../query/response/one-call-forecast';
import { Cities } from '../query/response/cities';
import { Rain } from '../model/rain';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {

  @Input() forecast: OneCallForecast;
  @Input() city: Cities;
  @Output() chart = new EventEmitter<OneCallForecast>();
  constructor() { }

  /* tslint:disable */
  ngOnInit() {
    this.forecast.current.dt *= 1000;
    this.forecast.current.sunrise *= 1000;
    this.forecast.current.sunset *= 1000;
    if (this.forecast.current.rain === undefined) {
      this.forecast.current.rain = 0;
    }
    this.forecast.daily.forEach(element => {
      element.dt *= 1000;
      element.sunrise *= 1000;
      element.sunset *= 1000;
      if (element.rain === undefined) {
        element.rain = 0;
      }
    });
    this.forecast.hourly.forEach(element => {
      element.dt *= 1000;
      element.sunrise *= 1000;
      element.sunset *= 1000;
      if (element.rain === undefined) {
        element.rain = 0;
      } else if (element.rain['1h'] === undefined) {
        element.rain = element.rain;
      } else {
        element.rain = element.rain['1h'];
      }
    });

  }

  sendGraph(city: OneCallForecast) {
    this.chart.emit(city);
  }

  datetime(dt: number) {
    let dat = new Date(dt);
    dat.setHours(dat.getHours() + 2);
    return dat.toUTCString().substr(0, dat.toUTCString().length - 4);
  }

  date(dt: number) {
    let dat = new Date(dt);
    dat.setHours(dat.getHours() + 2);
    return dat.toUTCString().substr(0, 11);
  }

  time(dt: number) {
    let dat = new Date(dt);
    dat.setHours(dat.getHours() + 2);
    return dat.toUTCString().substr(17, 5);
  }

}
