import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SunnyDaysComponent } from './sunny-days/sunny-days.component';


const routes: Routes = [
  {
    path: '',
    component: SunnyDaysComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
