# Sunny Days

Weather app. Human-Computer Interaction course project.

- **Technology**:
Angular 9.2.1 (latest)

- **Requirements**: npm/Node.js
(https://www.npmjs.com/get-npm)

- **Running the app**:
0.  Clone/download the repository from GitLab
1.  Open Command Prompt/PowerShell/Terminal as Administrator
2.  Change directory to project directory
3.  > npm install
4.  > ng serve
5.  After compiling, open http://localhost:4200
6.  Ignore errors
7.  Enjoy (hopefully)